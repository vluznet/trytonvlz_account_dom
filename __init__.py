# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# epository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import invoice
from . import sale
from . import invoice_authorization


def register():
    Pool.register(
        invoice_authorization.InvoiceAuthorization,
        invoice.Invoice,
        sale.Sale,
        party.Party,
        module='account_dom', type_='model')
