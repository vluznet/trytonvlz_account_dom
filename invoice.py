# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateReport, Button, StateView
from trytond.report import Report

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")


TYPE_INVOICE = [
    ('', ''),
    ('01', 'Facturas de Credito Fiscal'),
    ('02', 'Facturas de Consumo'),
    ('03', 'Notas de Debito'),
    ('04', 'Notas de Crédito'),
    ('12', 'Comprobantes de Compras'),
    ('13', 'Registro Unico de Ingresos'),
    ('14', 'Comprobante para Gastos Menores'),
    ('15', 'Comprobante para Regimenes Especiales'),
    ('16', 'Comprobantes para Exportaciones'),
    ('17', 'Comprobantes de Pagos al Exterior'),
    ('P', 'Comprobante de Venta Electronico'),
]


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    invoice_type = fields.Selection(TYPE_INVOICE, 'Clase Factura', states={
            'invisible': Eval('type') != 'out',
            'readonly': Eval('state').in_(['validated', 'posted', 'paid'])
        })
    invoice_type_string = invoice_type.translated('invoice_type')
    authorization = fields.Many2One('account.invoice.authorization',
        'Authorization', states={'readonly': False}, depends=['invoice_type']
    )

    @fields.depends('invoice_type', 'authorization')
    def on_change_invoice_type(self, name=None):
        Authorization = Pool().get('account.invoice.authorization')
        if self.invoice_type:
            authorizations = Authorization.search([
                ('kind', '=', self.invoice_type),
                ('state', '=', 'active'),
            ])
            if len(authorizations) == 1:
                authorization = authorizations[0]
                self.authorization = authorization.id
                # self.resolution = self.authorization.number
            else:
                self.authorization = None

    @classmethod
    def set_number(cls, invoices):
        pool = Pool()
        Authorization = pool.get('account.invoice.authorization')
        config = pool.get('account.configuration')(1)

        for invoice in invoices:
            if not invoice.number and invoice.type == 'out' \
                    and invoice.authorization:
                # invoice.check_authorization()
                invoice.number = invoice.authorization.sequence.get()
            invoice.save()
        super(Invoice, cls).set_number(invoices)


class PrintFormat606Start(ModelView):
    'Print Format 606 Start'
    __name__ = 'account_dom.print_format_606.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    period = fields.Many2One('account.period', 'Start Period', domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'])
    posted = fields.Boolean('Posted Move', help='Show only posted move')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PrintFormat606(Wizard):
    'Print Format 606 Start'
    __name__ = 'account_dom.print_format_606'
    start = StateView('account_dom.print_format_606.start',
        'account_dom.print_format_606_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('account_dom.print_format_606')

    def do_print_(self, action):
        if self.start.start_period:
            start_period = self.start.start_period.id
        else:
            start_period = None
        if self.start.end_period:
            end_period = self.start.end_period.id
        else:
            end_period = None

        if not self.start.party:
            party = None
        else:
            party = self.start.party.id

        start_account_id = None
        if self.start.start_account:
            start_account_id = self.start.start_account.id
        end_account_id = None
        if self.start.end_account:
            end_account_id = self.start.end_account.id

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'start_period': start_period,
            'end_period': end_period,
            'posted': self.start.posted,
            'start_account': start_account_id,
            'end_account': end_account_id,
            'party': party,
            'empty_account': self.start.empty_account,
            'reference': self.start.reference,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PrintFormat606Report(Report):
    __name__ = 'account_dom.auxiliary_book'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PrintFormat606Report, cls).get_context(records, data)
        # pool = Pool()
        # Account = pool.get('account.account')
        return report_context
