# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

TYPE_INVOICE = [
    ('', ''),
    ('01', 'Facturas de Credito Fiscal'),
    ('02', 'Facturas de Consumo'),
    ('03', 'Notas de Debito'),
    ('04', 'Notas de Crédito'),
    ('12', 'Comprobantes de Compras'),
    ('13', 'Registro Unico de Ingresos'),
    ('14', 'Comprobante para Gastos Menores'),
    ('15', 'Comprobante para Regimenes Especiales'),
    ('16', 'Comprobantes para Exportaciones'),
    ('17', 'Comprobantes de Pagos al Exterior'),
    ('P', 'Comprobante de Venta Electronico'),
]


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    invoice_type = fields.Selection(TYPE_INVOICE, 'Clase',
        states={
            'readonly': Eval('state').in_(['quote', 'confirmed', 'done'])
        })

    @staticmethod
    def default_invoice_type():
        return ''
