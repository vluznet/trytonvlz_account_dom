# This file is part of account module for Tryton.  The COPYRIGHT file at
# the top level of this repository contains the full copyright notices
# and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Not, Bool


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    id_number = fields.Char('Id Number')
    first_name = fields.Char('First Name', states={
        'invisible': Not(Bool(Eval('is_person'))),
    })
    last_name = fields.Char('Last Name', states={
        'invisible': Not(Bool(Eval('is_person'))),
    })
    type_document_dom = fields.Selection([
            ('', ''),
            ('cedula_identidad', 'Cedula Identidad Electoral'),
            ('pasaporte', 'Pasaporte'),
            ('cedula_extranjeria', 'Cedula Extranjeria'),
            ('rnc', 'RNC'),
        ], 'Document Type')
    type_person = fields.Selection([
            ('', ''),
            ('persona_fisica', 'Persona Fisica'),
            ('persona_juridica', 'Persona Juridica'),
        ], 'Tipo de Persona', states={'invisible': Not(Bool(Eval('is_person')))})
    subdivision = fields.Function(fields.Char('Subdivision Name'),
            'get_address')
    city = fields.Function(fields.Char('City Name'), 'get_address')
    street = fields.Function(fields.Char('Street'), 'get_address')
    vat_number = fields.Function(fields.Char('VAT Number'),
            'get_vat_number', searcher='search_vat_number')
    id_number_full = fields.Function(fields.Char('Full Id Number'),
            'get_id_number_full')
    vat_number_full = fields.Function(fields.Char('VAT Number Full'),
            'get_id_number_full')

    @fields.depends('is_person', 'name', 'first_name', 'last_name')
    def on_change_is_person(self):
        if self.is_person and self.name:
            first_name = self.name
            last_name = ''
            names = self.name.split(' ')
            if len(names) == 2:
                first_name = names[0]
                last_name = names[1]
            elif len(names) == 3:
                first_name = names[0]
                last_name = names[1] + ' ' + names[2]
            elif len(names) > 3:
                first_name = ' '.join(names[0: -2])
                last_name = ' '.join(names[-2:])
            self.first_name = first_name
            self.last_name = last_name

    def get_address(self, name):
        if name != 'street':
            attr = name[-4:]
            field = name[:-4] + 'code'
        else:
            field = name
            attr = ''
        for address in self.addresses:
            if hasattr(address, field):
                val = getattr(address, field)
                if name == 'subdivision':
                    return val.name
                if hasattr(val, attr):
                    return getattr(val, attr)
                return val
        return ''

    @classmethod
    def search_vat_number(cls, name, clause):
        return [
            ('identifiers.code',) + tuple(clause[1:]),
        ]

    def get_vat_number(self, name=None):
        if self.id_number:
            return self.id_number
        for identifier in self.identifiers:
            return identifier.code

    def get_id_number_full(self, name=None):
        res = self.id_number
        if self.id_number and self.id_number.isdigit():
            res = '{0:,}'.format(int(self.id_number)).replace(',', '.')
        # if self.check_digit is not None and self.type_document == '31':
        #     res = res + '-' + str(self.check_digit)
        return res
